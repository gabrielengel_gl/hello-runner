## How to

- Create GitLab Project
- Create GitLab PAT
- Create AWS access key
- set aws credentials & gtlb token as env variables
- Add GRIT example file
- Add GitLab Project ID
- Select a runner tag
- setup gitlab ci with grit clone

## Example

New pipeline
